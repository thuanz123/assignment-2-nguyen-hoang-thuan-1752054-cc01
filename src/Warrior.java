public class Warrior extends Fighter {
	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}   

	public double getCombatScore() {
		if(Utility.isPrime(Battle.GROUND))
			return 2 * ((double)getBaseHp());
		else {
			if(getWp() == 1)
				return (double)getBaseHp();
			
			return ((double)getBaseHp()) / 10.0;
		}
	} 		   
}
