public class Knight extends Fighter {
	public Knight(int baseHp, int wp) {
		super(baseHp, wp);
	}   

	public double getCombatScore() {
		if(Utility.isSquare(Battle.GROUND))
			return 2 * ((double)getBaseHp());
		else {
			if(getWp() == 1)
				return (double)getBaseHp();
			
			return ((double)getBaseHp()) / 10.0;
		}
	}
}
