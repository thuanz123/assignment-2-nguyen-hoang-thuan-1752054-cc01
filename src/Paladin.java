public class Paladin extends Knight {
	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}

	public double getCombatScore() {
		int fib1 = 1, fib2 = 1, temp;
		double n = 2;
		
		while(fib2 < getBaseHp()) {
			temp = fib1;
			fib1 = fib2;
			fib2 += temp;
			
			n++;
		}
		
		if(fib2 == getBaseHp() && n > 2)
			return 1000 + n;
		
		return 3 * ((double)getBaseHp());
	}
}
